# Imports
import pandas as pd
from sklearn.model_selection import train_test_split
import logging
from config import OUTPUT_SPLIT, OUTPUT_FEATURES


def split_dataset(data_df: pd.DataFrame) -> dict:
    """Just split the dataset with a dict"""

    # Splits the dataset
    print(data_df)
    X, y = data_df.drop("traffic_volume", axis=1), data_df.traffic_volume

    N = X.shape[0]  # Number of observation
    i_split = int(N * 0.7)  # Position of the split
    splits = X.iloc[:i_split], X.iloc[i_split:], y.iloc[:i_split], y.iloc[i_split:]

    # Transform to dict
    labels = ["X_train", "X_test", "y_train", "y_test"]
    splits_dict = dict(zip(labels, splits))

    return splits_dict


if __name__ == "__main__":
    input_df = pd.read_pickle(OUTPUT_FEATURES)  # Pulling data
    output_dict = split_dataset(input_df)

    pd.to_pickle(output_dict, OUTPUT_SPLIT)

    logging.info("The dataset has been split and stored at %s.", OUTPUT_SPLIT)
