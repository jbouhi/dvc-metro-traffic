# Imports
import pandas as pd
import logging
from config import OUTPUT_PREPARE


def prepare(data_df: pd.DataFrame) -> pd.DataFrame:
    data_df["date_time"] = pd.to_datetime(data_df["date_time"])
    data_df = data_df.set_index(data_df["date_time"])
    data_df = data_df.drop("date_time", axis=1)
    return data_df


if __name__ == "__main__":
    input_df = pd.read_csv("data/Metro_Interstate_Traffic_Volume.csv")  # Pulling data
    output_df = prepare(input_df)
    output_df.to_pickle(OUTPUT_PREPARE)

    logging.info("A dataset of %d columns with %d has been prepared and stored at %s.", output_df.shape[1],
                 output_df.shape[0], OUTPUT_PREPARE)
